const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');

const userRouter = require('./routes/userRoutes')
const productRouter = require('./routes/productRoutes')
const orderRouter = require('./routes/orderRoutes')

const app = express();
const port = 4000;
app.use(cors())

app.use(express.json());
app.use(express.urlencoded({extended: true}))

app.use('/api/users', userRouter)
app.use('/api/product', productRouter)
app.use('/api/order', orderRouter)

mongoose.connect("mongodb+srv://user:user123@cluster0.ok2rk.mongodb.net/ecommerce_api?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
})

mongoose.connection.once('open', () => console.log(`Now connected to MongoDB Atlas.`));

app.listen(process.env.PORT || port, () => {console.log(`Ecommerce-api is running on port ${process.env.PORT || port}`)
})