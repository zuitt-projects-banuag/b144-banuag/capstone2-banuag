const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({

	totalAmount : {
			type : Number,
			required: [true, "Total amount is required"]
		},
	purchasedOn : {
			type: Date,
			default: new Date()
		},
	userId: {
			type: String,
			required: [true, "userId is required"]
		},
	products: [
        {
            productId: {
                type: String,
                required: [true, "Product ID is required"]
            },
            productName: {
            	type: String,
            	required: [true, "Product Name is required"]
            },
            price: {
                type: Number,
                required: [true, "Price is required"]
            },
            quantity: {
            	type: Number,
            	required: [true, "Quality is required"]
            },
            isOrderUpdated: {
            	type: String,
            	default: true
            }
        }
    ]
	
	
})

module.exports = mongoose.model('Order', orderSchema)