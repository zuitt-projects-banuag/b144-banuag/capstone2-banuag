const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({

	 	name : {
			type : String,
			required : [true, "Name is required"]
		},
	 	description : {
			type : String,
			required : [true, "description is required"]
		},
		price : {
			type: Number,
			required : [true, "Price is required"]
		},
		isActive : {
			type : Boolean,
			default : true
		},
		createdOn : {
			type : Date,
			default: new Date()
		}	
	})

module.exports = mongoose.model('Product', userSchema)