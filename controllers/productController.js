const Product = require('../models/Product'); 
const bcrypt = require('bcrypt');
const auth = require('../auth');

//Add/Create a product list
module.exports.addProductList = (reqBody, userData) => {

	return Product.findById(userData.userId).then(result => {
	if(userData.isAdmin == false){
		return "You're not allowed to access this site, admin only"
	}else{
		let newProduct = new Product ({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false
			}else{
				return true
			}
		})

	  }

	})

}

//Active Product
module.exports.getAllActive = () => {
	return Product.find({isActive: true}).then(result => {
		return result;
	})
}

//Specific Product
module.exports.getProductOne = (reqParams) => {
	return Product.findById(reqParams.productId)
	.then(result => {
	return result;
	})
}

//Update a Product
module.exports.productUpdate = (reqParams, reqBody)=>{
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
    .then((product,error)=>{
		if(error){
			return false;
		}else{
			return "Product updated Successfully";
		}
	})
};

// Archive a Product
module.exports.productArchive = (reqParams, reqBody)=>{
	let archiveProduct ={
		isActive: false
	}
	return Product.findByIdAndUpdate(reqParams.productId, archiveProduct)
	.then((product, error)=>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
};


// All Product url(http://localhost:4000/api/product/productList)
module.exports.getProductList=()=>{
	return Product.find({}).then(result=>{
		return result;
	})
};

//-------------------HEROKU------------------------
//Create Product Sample Workflow
 module.exports.createProduct = (reqBody, userData) => {

	return Product.findById(userData.userId).then(result => {
	if(userData.isAdmin == false){
		return 'Only Admin can access this page!'
	}else{
			newProduct = new Product ({
			name : reqBody.name,
			description : reqBody.description,
			price : reqBody.price
		})

		return newProduct.save().then((product, error) => {
			if(error){
				return false
			}else{
				return true
			}
		})

	  }

	})

}

//Retrieve Single Product Sample Workflow
module.exports.sentProductOne = (reqBody) => {
	return Product.findById(reqParams.productId)
	.then(result => {
	return result;
	})
}

//Active Product
// module.exports.getAll = () => {
// 	return Product.find({isActive: true}).then(result => {
// 		return result;
// 	})
// }

//Update
module.exports.updateProduct = (reqParams, reqBody)=>{
	let updatedProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
    .then((product,error)=>{
		if(error){
			return false;
		}else{
			return "Product updated Successfully";
		}
	})
};

// Archive a Product
module.exports.archiveProduct = (reqParams, reqBody)=>{
	let archiveProduct ={
		isActive: false
	}
	return Product.findByIdAndUpdate(reqParams.productId, archiveProduct)
	.then((product, error)=>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
};
