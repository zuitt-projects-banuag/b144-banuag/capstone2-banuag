const User = require('../models/User');
const Product = require('../models/Product');
const Order = require('../models/Order');
const bcrypt = require('bcrypt');
const auth = require('../auth');

//Checking emails
module.exports.checkEmailExists = (reqBody) =>{
	return User.find({ email: reqBody.email }).then(result => {
		if(result.length > 0) {
			return true;
		} else{
			return false;
		}
	})
}

 //User Registration
 module.exports.registerUser = (reqBody) =>
 {
 	let newUser = new User ({

 		email: reqBody.email,
 		password: bcrypt.hashSync(reqBody.password, 10)
 	}) 

 	return newUser.save().then((user, error) => {
 		if(error){
 			return false;
 		} else{
 			return true
 		}
 	})
 }

 //Login
 module.exports.login = (reqBody) => {
 	return User.findOne({email:reqBody.email}).then(result => {
 		if(result == null){
 			return false;
 		}else{
 			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
 			if(isPasswordCorrect){
 				return { accessToken: auth.createAccessToken(result.toObject())}
 			}else{
 				return false
 			}
 		}
 	})
 }

 //Set User as Admin Sample Workflow
 module.exports.updateUser = (reqParams, reqBody) =>
 	{
 		let updatedUser = {
 			email: reqBody.email,
 			password: reqBody.password,
 			isAdmin: reqBody.isAdmin
 		}

 		return User.findByIdAndUpdate(reqParams.userId, updatedUser)
 		.then((user, err) => {
 			if(err){
 				return false
 			}else{
 				return "Successful updated!"
 			}
 		})
 	}

//Get all user
module.exports.getAllUser = (data) => {
	return User.find({}).then(result => {
		return result;
	})
}
