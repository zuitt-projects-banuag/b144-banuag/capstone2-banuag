const Order= require('../models/Order');
const Product = require('../models/Product')
const User = require('../models/User')
const bcrypt = require('bcrypt');
const auth = require('../auth');

//Non-admin User checkout
module.exports.addToCart = (reqBody, userData) => {


	return Product.findById(reqBody.productId).then(results => {
	if(userData.isAdmin == true){
		return false
	}else{
		let totalAmount = reqBody.quantity*results.price;

		let newOrder = new Order({
			userId : userData.userId,
			productId: reqBody.productId,
			quantity: reqBody.quantity,
			totalAmount: totalAmount
		})
		

		return newOrder.save().then((order, error) => {
			if(error){
				return false
			}else{
				return "Placed Order!"
			}
		})

	    }

	})
}

//User's Order
module.exports.getUserOrder = (reqBody, userData) => {
    return User.findById(userData.userId).then(result => {
        if (userData.isAdmin !== reqBody.userId) {
            return 'You cannot access this page'
        } else {
            return Order.findById(reqBody.orderId).then(result => {
                if (result !== null) {
                    return result
                } else {
                    return 'This order does not exist'
                }
            })
        }
    })
}

//Retrieve authenticated user’s orders
module.exports.getAllUserOrder=()=>{
	return Order.find({}).then(result=>{
		return result;
	})
};

//Retrieve all orders (Admin only)
module.exports.getAllOrder = () => {
    return Order.find().then((result, err) => {
        if (err) {
            return false;
        } else {
            return result;
        }
    })
};

