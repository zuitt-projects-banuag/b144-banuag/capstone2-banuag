const express = require("express");
const router = express.Router();
const productController = require("../controllers/productController");
const auth = require("../auth");

//Add a product
router.post('/productCreate', auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization) 

	productController.addProductList(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})

//Retrieve all active product
router.get('/', (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController));
})

//Retrieve Single Product
router.get('/:productId', (req,res) => {
	console.log(req.params.productId);
	productController.getProductOne(req.params).then(resultFromController=>res.send(resultFromController));
})

// Update a Product
router.put("/:productId", auth.verify, (req, res)=>{
	productController.productUpdate(req.params, req.body).then(resultFromController => res.send(resultFromController))
});

//Archive a Product
router.put("/:productId/archive", auth.verify,(req,res) => {
	productController.productArchive(req.params, req.body).then(result => res.send(result))
})

//All ProductList
router.post("/all", (req, res)=>{
	productController.getProductList().then(resultFromController=>res.send(resultFromController));
});

//-------------------HEROKU------------------------
//Create a product
router.post('/products', auth.verify,(req, res) => {

	const userData = auth.decode(req.headers.authorization) 

	productController.createProduct(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(resultFromController => res.send(resultFromController))
})


//Retrieve Single Product Sample Workflow
router.get('/:productId', auth.verify, (req, res) => {
	productController.sentProductOne().then(resultFromController=>res.send(resultFromController));
})

//Retrieve All Active Products Sample Workflow
// router.get('/active', (req, res) => {
// 	productController.getAll().then(resultFromController => res.send(resultFromController));
// })


module.exports = router;

//Update Product Sample Workflow
router.put("/:productId", auth.verify, (req, res)=>{
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
});

//Archive Product Sample Workflow
router.put("/:productId/archive", auth.verify,(req,res) => {
	productController.productArchive(req.params, req.body).then(result => res.send(result))
})