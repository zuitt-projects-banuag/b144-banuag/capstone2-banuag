const express = require('express');
const router = express.Router();
const mongoose = require('mongoose');
const orderController = require('../controllers/orderController');
const auth = require('../auth');

//Route for GET all orders from user
router.get("/:id", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization);
    orderController.getUserOrder(req.body, {
        userId: userData.id
    }).then(result => res.send(result))
})

//Non-admin User checkout
router.post('/users/checkout',(req, res) => {

    const userData = auth.decode(req.headers.authorization); 

    orderController.addToCart(req.body, {userId: userData.id, isAdmin:userData.isAdmin}).then(result=>res.send(result));
});


//Retrieve all orders (Admin only)
router.get('/allOrder', auth.verify, (req, res) => {
    orderController.getAllOrder().then(result => res.send(result))
});




module.exports = router;