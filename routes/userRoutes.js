const express = require('express');
const router = express.Router();
const auth = require("../auth");

const userController = require('../controllers/userController')

//Email Checking
router.post('/checkEmail', (req, res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})

//Routes for User Registration
router.post('/register', (req, res) => {
	userController.registerUser(req.body)
	.then(result => res.send(result));
})

//Routes for authenticating a user
router.post('/login', (req, res) => {
	userController.login(req.body).then(result => res.send(result))
})

//Update User
router.put('/:userId/setAdmin', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	
	userController.updateUser(req.params, req.body).then(result => res.send(result))
})

//Get all user
router.get("/allUser", auth.verify, (req, res) => {

	userController.getAllUser().then(result => res.send(result));
})

//Details of user
router.get("/details/",auth.verify, (req, res)=>{
	const userData = auth.decode(req.headers.authorization);
	userController.getProfile({userId : userData.id})
	.then(resultFromController => res.send(resultFromController))
});



module.exports = router;